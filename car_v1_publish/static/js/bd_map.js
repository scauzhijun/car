

// -------百度地图API调用相关功能开始--------//
 // 创建Map实例
var create_map_ins = function(dom){
  var map = new BMap.Map(dom);   
  map.centerAndZoom(new BMap.Point(114.404, 22.815), 12);  // 初始化地图,设置中心点坐标和地图级别
  //添加地图类型控件
  map.addControl(new BMap.MapTypeControl({
    mapTypes:[
            BMAP_NORMAL_MAP,
            BMAP_HYBRID_MAP
        ]}));   
  map.setCurrentCity("深圳");          // 设置地图显示的城市 此项是必须设置的
  map.enableScrollWheelZoom(true);
  map.enableAutoResize();

  var navigationControl = new BMap.NavigationControl({
    // 靠右上角位置
    anchor: BMAP_ANCHOR_TOP_RIGHT,
    // LARGE类型
    type: BMAP_NAVIGATION_CONTROL_LARGE,
    // 启用显示定位
    enableGeolocation: true,
    offset:new BMap.Size(0,55)
  });

  // 添加定位控件

var geolocationControl = new BMap.GeolocationControl();
  geolocationControl.addEventListener("locationSuccess", function(e){
    // 定位成功事件
    var address = '';
    address += e.addressComponent.province;
    address += e.addressComponent.city;
    address += e.addressComponent.district;
    address += e.addressComponent.street;
    address += e.addressComponent.streetNumber;
    alert("当前定位地址为：" + address);
  });
  geolocationControl.addEventListener("locationError",function(e){
    // 定位失败事件
    alert(e.message);
  });
  
  map.addControl(navigationControl);
  map.addControl(geolocationControl);

  return map
}


// 百度地图地址搜索功能
var search_map=function(mapobj,addr){
  // console.log(addr)
  var local = new BMap.LocalSearch(mapobj, {
    renderOptions:{map: map}
  });
  local.search(addr);
}

//百度地图添加设备marker覆盖物功能函数

var add_device_marker_bdmap=function(mapobj,obj,info_window='open'){
  
  var dev = obj.dev
  var point = new BMap.Point(0,0)
  if(dev == null){
    dev = {}
  }else{
    var lng = parseFloat(dev.lng)
    var lat = parseFloat(dev.lat)
    // 地址纠偏
    var lnglat = wgs84_bdmap(lat,lng)
    lng = lnglat[0]
    lat = lnglat[1]
    point = new BMap.Point(lng,lat)
    // 地址逆解析
    get_bdmap_addr(point,obj['device_id'])
  }
  var obj_name = obj.name
  var myIcon = new BMap.Icon(obj.marker_icon, new BMap.Size(50,50),{anchor : new BMap.Size(20, 42),imageSize:new BMap.Size(50, 50)});
  var marker = new BMap.Marker(point,{icon:myIcon});
  
  marker.self_detail=obj
  marker.addEventListener('click', function (e) {
    if(info_window=='open'){
      // open_device_infowindow(mapobj,e.target.self_detail,e.point)
      mapobj.openInfoWindow(e.target.info_window,e.point)
    }
  })
  mapobj.addOverlay(marker);//加载定位点
  marker.setTitle(obj_name);//设置标题
  var lb_html='<button class="layui-btn layui-btn-xs" style="border-radius:3px">'+obj_name+'</button>'
  var mylabel=new BMap.Label(obj_name,{offset:new BMap.Size(-3,-25)});
  mylabel.setStyle({border:'0',borderRadius:"3px"})
  marker.setLabel(mylabel);
  setTimeout(function(){
      marker.info_window = create_infowindow_bdmap(obj)
    },500)
  if(info_window=='open'){

    setTimeout(function(){
      
      mapobj.openInfoWindow(marker.info_window,point)
    },500)
  }
  mapobj.centerAndZoom(point, 16);
  return marker
};

var refresh_marker_bdmap = function(mapobj,marker,info,r){
  
  var point = marker.point
  
  var dev = info.dev
  if(dev == null){
    dev = {}
  }else{
    if(dev.lng != marker.self_detail.lng && dev.lat != marker.self_detail.lat){
      var lng = parseFloat(dev.lng)
      var lat = parseFloat(dev.lat)
      // 地址纠偏
      var lnglat = wgs84_bdmap(lat,lng)
      lng = lnglat[0]
      lat = lnglat[1]
      point = new BMap.Point(lng,lat)
      // 地址逆解析
      get_bdmap_addr(point,dev['device_id'])
      marker.setPosition(point)
      
    }
  }

  var open = true
  if(!marker.info_window.isOpen() && r){
    open = false
  }
  
  if(open){
    setTimeout(function(){
      marker.info_window = create_infowindow_bdmap(info)
      marker.info_window.disableAutoPan()
      mapobj.openInfoWindow(marker.info_window,point)
    },800)
  }
}


// 地址逆解析，从经纬度变成文字地址，百度接口
var all_addr_obj={};
var get_bdmap_addr=function(pt,key_field){
  var geoc = new BMap.Geocoder();  //地理位置逆解析
  var location
  geoc.getLocation(pt, function(rs){
    var addComp = rs.address
    if(rs.surroundingPois.length!=0){
      // console.log(rs)
       location=addComp+','+rs.surroundingPois[0]['title'] 
    }else{
       location=addComp
    }
    all_addr_obj[key_field]=location;

  });
};

var remove_marker_bdmap = function(mapobj,idx,marker_list){
  // console.log(idx,marker_list)
  var m = marker_list[idx]
  mapobj.closeInfoWindow()
  mapobj.removeOverlay(m)
  delete marker_list[idx]

}

// 画线功能
var draw_line_bdmap=function(mapobj,pts){
  console.log("draw_line_bdmap",mapobj,pts)
  var polyline = new BMap.Polyline(pts, {
      strokeColor:"#18a45b",//设置颜色
      strokeWeight:8 ,//宽度
      strokeOpacity:0.8,//折线的透明度，取值范围0 - 1
      enableEditing: false,//是否启用线编辑，默认为false
      enableClicking: false,//是否响应点击事件，默认为true
      // icons:[icons]
  });
  mapobj.addOverlay(polyline);
  mapobj.setViewport(pts);
  return polyline
}

// 手动路线和轨迹路线，需要进行均匀切割，获取指定步长的点，方便后台进行路线偏移计算，pts：点数组，step：步长
var get_point_of_polyline=function(pts,step){
  var res=[]
  res.push([pts[0].lng,pts[0].lat])
    // 获取两点距离
  function _getDistance(pxA, pxB) {
      return Math.sqrt(Math.pow(pxA.x - pxB.x, 2) + Math.pow(pxA.y - pxB.y, 2));
  };

  // 在转墨卡托坐标之后，本次移动的目标坐标点，初始坐标点的值+本次移动序号/总移动次数*（目标坐标点-初始坐标点）
  function effect(initPos, targetPos, currentCount, count) {
    var b = initPos, c = targetPos - initPos, t = currentCount,
    d = count;
    return c * t / d + b;
  }
  for(var i=0;i<pts.length-1;i++){
    var initPos=pts[i]
    var targetPos=pts[i+1]
      // 移动间隔时间
      timer = 10;
      //步长，米/次
      //初始坐标
      init_pos = map.getMapType().getProjection().lngLatToPoint(initPos);
      //获取结束点的(x,y)坐标
      target_pos = map.getMapType().getProjection().lngLatToPoint(targetPos);
      //两个点之间需要移动的总次数
      count = Math.round(_getDistance(init_pos, target_pos) / step);

      if(count==0){
        count=1
      }
      // 每两个点之间的距离大于步长的，进行均匀切割
      for(var n=1;n<count+1;n++){
        // console.log(n,count)
        var x = effect(init_pos.x, target_pos.x, n, count),
            y = effect(init_pos.y, target_pos.y, n, count),
            pos = map.getMapType().getProjection().pointToLngLat(new BMap.Pixel(x, y));
        res.push([pos.lng,pos.lat])
        //判断是否还继续递归
      }
  }

  console.log('end',res)
  return res

}

// 沿线平滑移动
var move_on_line=function(trail_opt,setSlider=''){
  var pts=trail_opt['tarckpoint']
  var mover=trail_opt['carMk']
  var point_num=pts.length
  var trail_text_num = 0
  var marker_move=function(i) {
    // console.log("marker_move-----------")
    var initPos=pts[i]
    var next = i+1
    var targetPos=pts[next]
    var point_num=pts.length;

    var g = targetPos['dev_datail']
    var text_idx = (i+1).toString()
    var trail_text_id = "trail_text_idx_"+text_idx
    $("#trail_detail_show").prepend(`<button id="`+trail_text_id+`" style="text-align: left;" onclick="click_detail_point('`+next+`')" class="layui-btn layui-btn-sm layui-btn-fluid">`+text_idx+`.里程:`+g.mileage+`公里,速度:`+g.speed+`公里/时,方向:`+g.direct+`°,位置:`+g.lng+","+g.lat+`,定位时间:`+g.track_ts+`,接收时间:`+g.recv_ts+`</button><br>`)

    // 获取两点距离
    function _getDistance(pxA, pxB) {
        return Math.sqrt(Math.pow(pxA.x - pxB.x, 2) + Math.pow(pxA.y - pxB.y, 2));
    };
    // wgs84坐标转换为墨卡托坐标
    function _getMercator(poi) {
        return map.getMapType().getProjection().lngLatToPoint(poi);
    };
    // 在转墨卡托坐标之后，本次移动的目标坐标点，初始坐标点的值+本次移动序号/总移动次数*（目标坐标点-初始坐标点）
    function effect(initPos, targetPos, currentCount, count) {
      var b = initPos, c = targetPos - initPos, t = currentCount,
      d = count;
      return c * t / d + b;
    }
    //当前的帧数
    currentCount = 0;
    // 移动间隔时间
    timer = 10;
    //步长，米/次
    step = trail_opt['play_speed'] / (1000 / timer);
    //初始坐标
    init_pos = map.getMapType().getProjection().lngLatToPoint(initPos);
    
    //获取结束点的(x,y)坐标
    target_pos = map.getMapType().getProjection().lngLatToPoint(targetPos);
    //两个点之间需要移动的总次数
    count = Math.round(_getDistance(init_pos, target_pos) / step);
    // console.log("initPos",init_pos,target_pos,count)
    // return
    if(setSlider!=''){
      var perc = ((i+1)/point_num).toFixed(2)
      // console.log(perc,(i+1)/point_num)
      setSlider(perc, 'trail_progress');
    }
    intervalFlag = setInterval(function() {
    //两点之间当前帧数大于总帧数的时候，则说明已经完成移动
      if (currentCount >count | count < 1) {
          clearInterval(intervalFlag);
          i++
          // 当前运行的轨迹点做自增加记录
          trail_opt['current_pt']++;
          // console.log(i,trail_opt,point_num)
          if(i>point_num){

            return 
          }
          move_next(i)
          
          
      }else {
        currentCount++;
        var x = effect(init_pos.x, target_pos.x, currentCount, count),
            y = effect(init_pos.y, target_pos.y, currentCount, count),
            pos = map.getMapType().getProjection().pointToLngLat(new BMap.Pixel(x, y));
        // 设置marker
        if(currentCount == 1){
            var pt_label=mover.getLabel()
            pt_label.setContent(initPos['html']);
            mover.setLabel(pt_label);

            if(!map.getBounds().containsPoint(pos)){
              map.panTo(pos);
            } 
        }
        //正在移动
        // console.log(targetPos['dev_datail'].direct)
        
        mover.setRotation(targetPos['dev_datail'].direct)
        // console.log("pos",pos)
        mover.setPosition(pos);
        
        

        //设置自定义overlay的位置
    }
  },timer);
  };
  // 递归判断条件，上一个点移动完毕，判断是否还有下一个点需要移动
  var move_next=function(num){
    if(num <= point_num-2 && trail_opt['status']=='run'){
      marker_move(num)
    }else{
      if(trail_opt['current_pt']>=point_num-2){
        trail_opt['current_pt']=0
        trail_opt['status']='stop'
      }
      if(setSlider!=''){
        setSlider((1), 'trail_progress');
      }
      layer.msg("轨迹播放完毕")
      console.log("轨迹播放完毕")
    }
  }

  // 控制
  // 停止状态和首次运行状态，启动轨迹
  if(trail_opt['status']=='f_run' | trail_opt['status']=='stop'){
    trail_opt['status']='run';
    trail_opt['current_pt']=0
    marker_move(0)
  // 正在运行轨迹的状态，点击play无效
  }else if(trail_opt['status']==='run'){
    return
  // 按下暂停，记录暂停状态，再点击播放，从暂停处开始播放
  }else{
    if(trail_opt['current_pt']>=point_num){
      trail_opt['current_pt']=0
    }
    trail_opt['status']='run'
    marker_move(trail_opt['current_pt'])
  }
}


// 点击轨迹点展示相应信息 
var click_detail_point = function(d){
  var pts=trail_opt['tarckpoint']
  var g = pts[d].dev_datail
  
  var car_name = device_obj[g.device_id].user_name
  var carinfo = JSON.stringify(car_obj[car_name])
  carinfo = JSON.parse(carinfo)
  carinfo.dev = g
  refresh_marker_bdmap(map,carMk,carinfo)
  setTimeout(function(){
    pts[d].dev_datail.location = all_addr_obj[g.device_id]
    var trail_text_id = "trail_text_idx_"+d
    console.log("click_detail_point",trail_text_id,pts[d].dev_datail.location)
    $("#"+trail_text_id).text(d+`.里程:`+g.mileage+`公里,速度:`+g.speed+`公里/时,方向:`+g.direct+`°,位置:`+all_addr_obj[g.device_id]+`,时间:`+g.track_ts+`,接收时间:`+g.recv_ts)

  },1000)
} 

//设备定位点信息窗展示功能

var car_window_htm = function(obj){
  // console.log("car_infowindow",obj)
  var detail = eval("("+obj.detail+")")
  var apply = obj.apply
  var status = ''
  var applyinfo = ''
  var status_map = {"":"空闲","apply":"已申请,未领用","get_key":"使用中"}
  if(apply){
    status = status_map[obj.status]
    applyinfo = "申请人"+obj.applyer_name+","+"目的:"+apply.purpose+","+"乘客:"+apply.passenger
  }
  
  var htm = 

  "<div style='margin-top:10px;'>"+
    "<table class='layui-table' lay-size='sm'  style='font-size:10px;margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;'>"+
    "<tbody>"+
    "<tr>"+
      "<td bgcolor='#009688' style='height:15px'><font color='white'>车牌:"+obj.name+"</font></td>"+
    "</tr>"+
    "<tr >"+
    "<td style='height:15px'><font>"+"型号 : "+detail.model+"</font></td>"+
    "</tr>"+
    "<tr >"+
    "<td style='height:15px'><font>"+"部门 : "+obj.department+"</font></td>"+
    "</tr>"+
    "<tr >"+
    "<td style='height:15px'><font>"+"状态 : "+status+"</font></td>"+
    "</tr>"+
    "<tr >"+
    "<td style='height:15px'><font>"+"使用 : "+applyinfo+"</font></td>"+
    "</tr>"+
  "</table>"+
  "</div>"
  ;
  return htm
}

var device_window_htm = function(gps){
  var device_id = gps.device_id
  var dev_loca=all_addr_obj[device_id]
  // var status_list=['acc_off', 'ante_ok', 'undefence', 'normal','online','sleep']
  status_dict={'undefence':'未设防', 
                'normal':'正常',
                'online':'<font color="green">在</font>',
                'sleep':'<font color="blue">休</font>',
                'sos_alarm':'<font color="red">sos</font>',
                'offline':'<font color="black">离</font>'}
  var rssi=gps.rssi
  var gps_num = gps.gps
  var bds_num = gps.bds
  
  var stop_time= 0
  
  var mileage=gps.mileage
  if(mileage==null){
    mileage=''
  }

  var battery = gps.battery 
  if (battery == null){
    battery = "0"
  }

  var status = get_device_status(gps).status
  var status = get_device_status(gps).status
  if(status ==='on_line'){
    status=`<p style="color:green;font-size:10px">在</p>`
  }else{
    status=`<p style="font-size:10px">离</p>`
  }
  var alarm = ""
  if (gps.alarm != "" && gps.alarm != null){
    alarm = `<p style="color:red;font-size:10px">警</p>`
  }

  var info_window_baidu=
   "<div style='margin-top:10px;'>"+
    "<table class='layui-table' lay-size='sm'  style='font-size:10px;margin:0px 0px 0px 0px;padding:0px 0px 0px 0px;'>"+
    "<tbody>"+
    "<tr>"+
      "<td colspan=2 bgcolor='#009688' style='height:15px'><font color='white'>"+"设备ID : "+device_id+"</font></td>"+
    "</tr>"+
    "<tr>"+
    "<td colspan=2>"+
    "<div class='layui-row'>"+
    "<div class='layui-col-md3'>"+status+" "+alarm+"</div>"+
    "<div class='layui-col-md9'>"+
    "<i style = 'font-size:8px;margincolor: #009688;' >电池:"+battery+"</i>"+
    "<i style = 'font-size:8px;margin-left:5px; margincolor: #009688;'>信号:"+rssi+"</i>"+
    "<i style = 'font-size:8px;margin-left:5px; margincolor: #009688;'>gps:"+gps_num+"</i>"+
    "<i style = 'font-size:8px;margin-left:5px;  margincolor: #009688;'>北斗:"+bds_num+"</i>"+
    "</div>"+
    "</div>"+
    "</td>"+
    "<tr>"+"<td>速度："+gps.speed+" km/h</td>"+"<td>里程 : "+mileage+" 公里</td>"+
    "</tr>"+
    "<tr>"+"<td>停留："+stop_time+" 小时</td>"+"<td>定位 : "+gps.track_ts+"</td>"+
    "</tr>"+
    "<tr>"+
    "<td colspan=2>上报："+gps.recv_ts+"</td>"+
    "</tr>"+
    "<tr>"+
    "<td colspan=2>位置："+all_addr_obj[device_id]+"</td>"+
    "</tr>"+

      "</tbody>"+
  "</table>"+ 
    "<br>"+
    "<button class='layui-btn layui-btn-xs' onclick='track_modal(\""+gps.user_name+"\")'>"+
      '<i class="layui-icon ">&#xe64c;</i>'+
      "追踪"+
    "</button>"+
  "</div>"
  ;

  return info_window_baidu
}


var create_infowindow_bdmap=function(obj){

  var htm = car_window_htm(obj)
  var gps = obj.dev
  //信息窗口参数722,1536
  var opts = {
    width : get_width/6,     // 信息窗口宽度
    height: get_height/4.5,     // 信息窗口高度
    offset:new BMap.Size(0, -32)
  };

  if(gps){
    htm+=device_window_htm(gps)
    opts = {
      width : get_width/5,     // 信息窗口宽度
      height: get_height/1.8,     // 信息窗口高度
      offset:new BMap.Size(0, -32)
    };
  }

  var gpsinfoWindow = new BMap.InfoWindow(htm, opts,{anchor : new BMap.Size(20, 42)});
  return gpsinfoWindow
};


//简易信息窗口
var open_simple_infowindow=function(tp){
  //信息窗口参数
  var gps_num=tp.gps,bds_num=tp.bds;
  if(gps_num==null){
    gps_num='0'
  }
  if(bds_num==null){
    bds_num='0'
  }

  var opts = {
    width : 30,     // 信息窗口宽度
    height: 200,     // 信息窗口高度
    };

  var p=
    "<div style='margin:0;padding:0;'>"+
      "<table class='layui-table' lay-size='sm' lay-skin='nob' style='margin:0;padding:0'>"+
        "<tr>"+
          "<td colspan=2>"+
          "<div class='layui-btn-group'>"+
            "<i class='fa fa-battery-full pull-right' style = 'font-size:10px; padding-right: 9px;color: #009688;' > "+tp.battery+"</i>"+
            "<i class='fa fa-broadcast-tower pull-right'  style = 'font-size:10px; padding-right: 9px;color: #009688;'>"+tp.rssi+"</i>"+
            "<i class='fa fa-satellite-dish pull-right'  style = 'font-size:10px; padding-right: 9px;color: #009688;'>"+gps_num+"</i>"+
            "<i class='fa fa-satellite pull-right'  style = 'font-size:10px; padding-right: 1px;color: #009688;'>"+bds_num+"</i>"+
          "</div>"+
          "</td>"+
        "</tr>"+
        "<tr>"+
          "<td>"+
            "里程："+tp.mileage+"公里"+
          "</td>"+
          "<td>"+
            "速度："+tp.speed+"km/h"+
          "</td>"+
        "</tr>"+
        "<tr>"+
         "<td>"+
            "方向："+tp.dirct+"°"+
          "</td>"+
          "<td>"+
            "类型："+tp.track_type+
          "</td>"+
        "</tr>"+
        "<tr>"+
          "<td colspan='2'>"+
            "时间："+tp.serv_receive+
          "</td>"+
        "</tr>"+
        "<tr>"+
          "<td colspan='2'>"+
            "位置："+all_addr_obj[tp.device_id]+
          "</td>"+
        "</tr>"+
      "</table>"+
    "</div>";
    var gpsinfoWindow = new BMap.InfoWindow(p, opts);

    // gpsinfoWindow.open()
    var point=new BMap.Point(tp.lng,tp.lat)
    map.openInfoWindow(gpsinfoWindow,point);    
};

